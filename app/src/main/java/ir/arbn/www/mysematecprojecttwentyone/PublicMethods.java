package ir.arbn.www.mysematecprojecttwentyone;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * Created by A.R.B.N on 2/27/2018.
 */
public class PublicMethods {
    public static void notification(Context mContext, String title, String content) {
        Intent intent = new Intent(mContext, HomeActivity.class);
        PendingIntent pi = PendingIntent.getActivity(mContext, 0, intent, 0);
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "Category Name",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("description of Category");
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext,
                "default")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(content)// message for notification
                .setContentIntent(pi)
                .setAutoCancel(true); // clear notification after click
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());

    }
}
